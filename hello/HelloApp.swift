//
//  helloApp.swift
//  hello
//
//  Created by Itamar Sousa Silva on 23/11/20.
//

import SwiftUI
import Firebase

@main
struct HelloApp: App {
    
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
