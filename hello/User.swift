//
//  User.swift
//  hello
//
//  Created by Itamar Sousa Silva on 10/12/20.
//

import Foundation

struct User {
    
    let name: String
    let email: String
    
    var isValid: Bool {
        return !name.isEmpty && !email.isEmpty
    }
}
