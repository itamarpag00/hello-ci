//
//  ContentView.swift
//  hello
//
//  Created by Itamar Sousa Silva on 23/11/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        var a = "add"
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

class SomeClass {
    var lookUp = false
    func lookup(){ }        // Noncompliant; method name differs from field name only by capitalization
    func lookUP(){ }        // Noncompliant; method name differs from field and another method name only by capitalization
}
