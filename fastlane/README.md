fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios signin
```
fastlane ios signin
```
Download profiles
### ios rematch
```
fastlane ios rematch
```
Renew provisionning profiles
### ios device
```
fastlane ios device
```
Add new device
### ios bump
```
fastlane ios bump
```
Increment build version and commit
### ios patch
```
fastlane ios patch
```
Increment patch version and commit
### ios minor
```
fastlane ios minor
```
Increment minor version and commit
### ios major
```
fastlane ios major
```
Increment major version and commit
### ios building_time
```
fastlane ios building_time
```
Profiling building times
### ios testing
```
fastlane ios testing
```
Run test for scheme
### ios alpha
```
fastlane ios alpha
```
Generate a development build on App Distribution
### ios beta
```
fastlane ios beta
```
Generate a prodcution build on TestFlight
### ios metrics
```
fastlane ios metrics
```
Gerenerate quality metrics

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
